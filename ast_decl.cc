/* File: ast_decl.cc
 * -----------------
 * Implementation of Decl node classes.
 */
#include "ast_decl.h"
#include "ast_type.h"
#include "ast_stmt.h"
#include "semantic_analyzer.h"
#include "errors.h"
#include <string.h>
#include <stdio.h>

Decl::Decl(Identifier *n) : Node(*n->GetLocation())
{
    Assert(n != NULL);
    (id = n)->SetParent(this);
}


VarDecl::VarDecl(Identifier *n, Type *t) : Decl(n)
{
    Assert(n != NULL && t != NULL);
    (type = t)->SetParent(this);
}

void VarDecl::PrintChildren(int indentLevel)
{
    if (type) { type->Print(indentLevel + 1); }
    if (id) { id->Print(indentLevel + 1); }
}

void VarDecl::CheckSub()
{
    VarDecl *prevDecl = Scp::curr->getVarCurrScope(id);
    // FnDecl *prevFnDecl = Scp::fnTab[this->getIDName()];
    FnDecl *prevFnDecl = Scp::curr->getFn(this->getIDName());

    if (prevDecl != NULL) {
        //this->setType(new Type("error"));
        // Scp::curr->replaceWithErrorSymTab(this, prevDecl->getType());
        Scp::curr->insertToSymTabNoChecks(this);
        ReportError::DeclConflict(this, prevDecl);
    } else if (prevFnDecl) {
        ReportError::DeclConflict(this, prevFnDecl);
    } else {
        // cout << "No error found in variable declaration." << endl;
        Scp::curr->insertToSymTab(this);
    }



    // VarDecl *last_declaration = Scp::curr->getVar(id);
    // if (last_declaration != NULL) { // Check if var has already been declared
    //     ReportError::DeclConflict(this, last_declaration);
    //     // type = new Error();
    // } else {
    //     Scp::curr->insertToSymTab(this);
    // }
}

FnDecl::FnDecl(Identifier *n, Type *r, List<VarDecl *> *d) : Decl(n)
{
    Assert(n != NULL && r != NULL && d != NULL);
    (returnType = r)->SetParent(this);
    (formals = d)->SetParentAll(this);
    body = NULL;
}

void FnDecl::SetFunctionBody(Stmt *b)
{
    (body = b)->SetParent(this);
}

void FnDecl::PrintChildren(int indentLevel)
{
    if (returnType) { returnType->Print(indentLevel + 1, "(return type) "); }
    if (id) { id->Print(indentLevel + 1); }
    if (formals) { formals->PrintAll(indentLevel + 1, "(formals) "); }
    if (body) { body->Print(indentLevel + 1, "(body) "); }
}

void FnDecl::CheckSub()
{
    // VarDecl *last_declaration = Scp::curr->getVarCurrScope(this->getIdentifier());

    // if (last_declaration != NULL) {
    //     ReportError::DeclConflict(this, last_declaration);
    // }
    //creating new scope for a function declaration
    Scp::curr->insertToFnTab(this);
    Scp *fnScp = new Scp();
    // cout << "setting flag: ShouldntCreateNewScope to true inside of FnDecl::CheckSub()\n";
    fnScp->setShouldntCreateNewScope(true);
    // cout << "Creating a new Scp in FnDecl::CheckSub() for function: " << this->getIDName() << endl;
    //adding all formals to symbol table
    // cout << "Calling body->CheckSub() in FnDecl::CheckSub()" << endl;
    // fnScp->insertToFnTab(this);
    for (int i = 0; i < formals->NumElements(); i++) {
        fnScp->insertToSymTab(formals->Nth(i));
    }
    // cout << "returnType of " << this->getIDName() << ": " << returnType->getName() << endl;
    fnScp->setRetType(returnType);
    body->CheckSub();
    //setting the return type of the scope to the return type of the function
    if (!fnScp->getRetExists() && (strcmp(fnScp->getRetType()->getName(), "void") != 0)) {
        ReportError::ReturnMissing(this);
    }
}

