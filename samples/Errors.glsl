//test for multiplly declared functions
/*int foo() {return 1;} 
int foo() {return 2;}
*/
/*int foo()
{
	x++;
	x =1;
	float x;
	return 2;
}*/

/*int foo()
{
	x++; //error
	x =1; //no error
	float x; //error 
	return 2;
}*/

void foo(){
int i;
bool y;
for (i=0; y = true ; i++) {
 
}
}

//test for variable redeclaration and not propogating error 
/*void main() {
   bool x;
   int x;
   float x;
	x = 5.5;
	x++;
	 
}*/

//test for incompatible operands 
/*void bar(int x)
{
	x++; //OK
	bool y;
	y = true;
	y++; //error 
}*/


//test to check overloaded function, should give no error 
/*void foo(int x)
{
	
}

void foo(bool x)
{
	
}*/

//test to check overloaded function, should give no error 
/*void foo(int y)
{
	
}

void foo(int x, int y )
{
	return; 	
}
void foo()
{
	
}
void foo()
{

	
}
void foo()
{}*/

//test nested function declarations
/*void foo(int x)
{
	void foo(bool x)
	{
		
	}
	
}*/
