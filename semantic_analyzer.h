#include "list.h"
#include <list>
#include <unordered_map>
#include <string>
#include <utility>
#include "ast.h"
#include "ast_decl.h"
#include "errors.h"
#include <vector>

using std::unordered_map;
using std::list;
using std::string;

class Scp {
protected:
    Scp *prev;
    Type *retType;
    bool retExists;
    bool canBreak;
    bool canContinue;
    bool shouldntCreateNewScope;
    bool inside_of_default_stmt;
    bool inside_of_case_stmt;
    bool has_break_inside_case;
    bool in_switch;
    unordered_map<string, VarDecl *> symTab;

public:
    static Scp *curr;
    static unordered_map<string, vector<FnDecl *>> fnTab;

    Scp() : retExists(false), canBreak(false), canContinue(false), shouldntCreateNewScope(false), inside_of_default_stmt(false), inside_of_case_stmt(false), has_break_inside_case(false), in_switch(false)
    {
        symTab = unordered_map<string, VarDecl *>();
        Type *return_type;
        bool isGlobalScope = false;
        if (NULL != Scp::curr) {
            return_type = Scp::curr->getRetType();
            isGlobalScope = true;
        }
        this->prev = Scp::curr;
        Scp::curr = this;
        if (isGlobalScope) {
            this->setRetType(return_type);
        }
    }

    ~Scp()
    {
        if (this->prev) {
            Scp::curr = this->prev;
        }
    }

    bool getIn_switch()
    {
        return in_switch;
    }

    void setIn_switch(bool b)
    {
        in_switch = b;
    }

    bool getInside_of_case_stmt()
    {
        return inside_of_case_stmt;
    }

    void setInside_of_case_stmt(bool b)
    {
        inside_of_case_stmt = b;
    }

    bool getInside_of_default_stmt()
    {
        return inside_of_default_stmt;
    }

    bool getHas_break_inside_case()
    {
        return has_break_inside_case;
    }

    void setInside_of_default_stmt(bool b)
    {
        inside_of_default_stmt = b;
    }

    void setHas_break_inside_case(bool b)
    {
        has_break_inside_case = b;
    }

    bool getShouldntCreateNewScope()
    {
        return shouldntCreateNewScope;
    }

    void setShouldntCreateNewScope(bool flag)
    {
        shouldntCreateNewScope = flag;
    }

    bool getCanBreak()
    {
        return canBreak;
    }

    bool getCanContinue()
    {
        return canContinue;
    }

    void setCanBreak(bool isBreak)
    {
        canBreak = isBreak;
    }

    void setCanContinue(bool isContinue)
    {
        canContinue = isContinue;
    }

    Scp *getPrev()
    {
        return prev;
    }

    Type *getRetType()
    {
        return retType;
    }

    bool getRetExists()
    {
        return retExists;
    }

    void setRetType(Type *newReturn)
    {
        retType = newReturn;
    }
    void setRetExists(bool exists)
    {
        retExists = exists;
    }

    Type *getNodeType(Identifier *ident)
    {
        VarDecl *declaration = getVar(ident);
        if (declaration != NULL) {
            return declaration->getType();
        } else {
            return  NULL;
        }
        // return ((declaration != NULL) : declaration->getType() ? NULL);
    }

    VarDecl *getVarCurrScope(Identifier *ident)
    {
        auto node = symTab.find(ident->getName());
        if (node == symTab.end()) {
            return NULL; //var not found in curr scope
        } else {
            return node->second; //var found in curr scope
        }
        // return ((node == symTab.end()) : (prev != NULL : getVar(ident) ? NULL) ? node->second);
    }

    FnDecl *getFn(string key)
    {
        auto node = Scp::fnTab.find(key);
        if (node == Scp::fnTab.end()) {
            return NULL; //var not found in curr scope
        } else {
            //return first occurance in vector
            return node->second.front(); //var found in curr scope
        }
        // return ((node == symTab.end()) : (prev != NULL : getVar(ident) ? NULL) ? node->second);
    }

    VarDecl *getVar(Identifier *ident)
    {
        // cout << "Entering Scp::getVar()\n";
        auto node = symTab.find(ident->getName());
        if (node == symTab.end()) {
            if (prev != NULL) {
                return prev->getVar(ident);
            } else {
                return NULL;
            }
        } else {
            return node->second;
        }
        // return ((node == symTab.end()) : (prev != NULL : getVar(ident) ? NULL) ? node->second);
    }

    void insertToSymTab(VarDecl *decl)
    {
        auto symNode = symTab.find(decl->getIDName());
        if (symNode == symTab.end()) {
            symTab.insert(std::make_pair<string, VarDecl *>(decl->getIDName(), decl));
        } else {
            ReportError::DeclConflict(decl, symNode->second);
        }

        return;
    }

    void insertToFnTab(FnDecl *fnDecl)
    {
        // cout << "fnDecl name is: " << fnDecl->getIDName() << " in Scp::insertToFnTab()\n";

        auto fnNode = Scp::fnTab.find(fnDecl->getIDName());
        auto symNode = symTab.find(fnDecl->getIDName());
        if (fnNode == Scp::fnTab.end() && symNode == symTab.end()) {
            // cout << "Inserting to fndecl in Scp::insertToFnTab()\n";

            //first time inserting key in fnTab
            vector<FnDecl *> v;
            v.push_back(fnDecl);
            Scp::fnTab.insert(std::make_pair<string, vector<FnDecl *>>(fnDecl->getIDName(), v));
        } else if (fnNode != Scp::fnTab.end()) {
            //to allow function overloading:
            List<VarDecl *> *passedInFormals = fnDecl->getFormals();
            Type *passedInRetType = fnDecl->getReturnType();
            vector<FnDecl *> vec = fnNode->second;
            bool formalsDifferent = false;

            //must make sure that each fndecl in the vector is differen from the one we want to insert
            for (int it = 0; it < vec.size(); it++) {

                List<VarDecl *> *existFormals = vec[it]->getFormals();
                //check if sizes are not equivalent and start checking next fndecl if they aren't b/c diff fn signatures
                int existSize = existFormals->NumElements();
                int passedInSize = passedInFormals->NumElements();
                if (existSize != passedInSize) {
                    //continue to check next fnDecl in vector since this one differs
                    continue;
                }

                formalsDifferent = false;

                for (int i = 0; i < existSize && i < passedInSize; i++) {
                    if (existFormals->Nth(i)->getType()->getNameStr() !=
                            passedInFormals->Nth(i)->getType()->getNameStr()) {

                        //continue to check next fnDecl in vector since this one differs
                        formalsDifferent = true;
                        break;
                    }
                }
                if (formalsDifferent) {
                    continue;
                }

                Type *existRetType = vec[it]->getReturnType();
                if (passedInRetType->getNameStr() != existRetType->getNameStr()) {

                    //continue to check next fnDecl in vector since this one differs
                    continue;
                }
                // cout << "FnTab Conflict, not inserting fndecl in Scp::insertToFnTab()\n";
                ReportError::DeclConflict(fnDecl, vec[it]);
                return;

            }
            //looped through all of fndecls and didn't find one that was the same, so can push
            //the current fndecl to vector
            // cout << "Inserting to fndecl b/c differs from all other fndecls in vector in Scp::insertToFnTab()\n";
            Scp::fnTab[fnDecl->getIDName()].push_back(fnDecl);

        } else { /* if (symNode != Scp::fnTab.end()) */
            /* if (symNode != Scp::fnTab.end()) */

            // cout << "SymTab Conflict, not inserting fndecl in Scp::insertToFnTab()\n";
            ReportError::DeclConflict(fnDecl, symNode->second);
        }

        return;
    }

    // void replaceWithErrorSymTab(VarDecl *decl)
    // {
    //     auto node = symTab.find(decl->getIDName());
    //     if (node == symTab.end()) {
    //         return; //var not found in curr scope, this should NEVER be the case
    //     } else {
    //         node->second->setType(new Type ("error")); //vardecl's type set to error
    //     }

    //     return;
    // }

    void insertToSymTabNoChecks(VarDecl *decl)
    {
        symTab[decl->getIDName()] = decl;
        return;
    }






};
