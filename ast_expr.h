/* File: ast_expr.h
 * ----------------
 * The Expr class and its subclasses are used to represent
 * expressions in the parse tree.  For each expression in the
 * language (add, call, New, etc.) there is a corresponding
 * node class for that construct.
 *
 * pp3: You will need to extend the Expr classes to implement
 * semantic analysis for rules pertaining to expressions.
 */


#ifndef _H_ast_expr
#define _H_ast_expr

#include "ast.h"
#include "ast_stmt.h"
#include "list.h"
#include "semantic_analyzer.h"
#include "errors.h"
#include "ast_decl.h"

void yyerror(const char *msg);

class Expr : public Stmt {
public:
    Expr(yyltype loc) : Stmt(loc) {}
    Expr() : Stmt() {}
    virtual Type *CheckExpr() = 0;
    virtual bool checkLVal()
    {
        return false;
    }
    void CheckSub()
    {
        CheckExpr();
    }
    friend std::ostream &operator<< (std::ostream &stream, Expr *expr)
    {
        return stream << expr->GetPrintNameForNode();
    }
};

class ExprError : public Expr {
public:
    ExprError() : Expr() { yyerror(this->GetPrintNameForNode()); }
    const char *GetPrintNameForNode() { return "ExprError"; }
    Type *CheckExpr()
    {
        return NULL;
    }
};

/* This node type is used for those places where an expression is optional.
 * We could use a NULL pointer, but then it adds a lot of checking for
 * NULL. By using a valid, but no-op, node, we save that trouble */
class EmptyExpr : public Expr {
public:
    const char *GetPrintNameForNode() { return "Empty"; }
    Type *CheckExpr()
    {
        return NULL;
    }
};

class IntConstant : public Expr {
protected:
    int value;

public:
    Type *CheckExpr()
    {
        return new Type("int");
    }
    IntConstant(yyltype loc, int val);
    const char *GetPrintNameForNode() { return "IntConstant"; }
    void PrintChildren(int indentLevel);
};

class FloatConstant: public Expr {
protected:
    double value;

public:
    Type *CheckExpr()
    {
        return new Type("float");
    }
    FloatConstant(yyltype loc, double val);
    const char *GetPrintNameForNode() { return "FloatConstant"; }
    void PrintChildren(int indentLevel);
};

class BoolConstant : public Expr {
protected:
    bool value;

public:
    Type *CheckExpr()
    {
        return new Type("bool");
    }
    BoolConstant(yyltype loc, bool val);
    const char *GetPrintNameForNode() { return "BoolConstant"; }
    void PrintChildren(int indentLevel);
};

class VarExpr : public Expr {
protected:
    Identifier *id;

public:
    VarExpr(yyltype loc, Identifier *id);
    const char *GetPrintNameForNode() { return "VarExpr"; }
    void PrintChildren(int indentLevel);
    Type *CheckExpr()
    {
        // cout << "In VarExpr::CheckExpr()\n";
        Type *type = Scp::curr->getNodeType(id);
        if (type) {
            // cout << "type is " << type->getName() << " in VarExp::CheckExpr()\n";
            return type;
        } else {
            // Scp::curr->insertToSymTab(new VarDecl(id, new Type("error")));
            ReportError::IdentifierNotDeclared(id, LookingForVariable);
            return new Type("error");
        }
    }
};

class Operator : public Node {
protected:
    char tokenString[4];

public:
    Operator(yyltype loc, const char *tok);
    string getTokStr() {return string(tokenString);}
    const char *GetPrintNameForNode() { return "Operator"; }
    void PrintChildren(int indentLevel);
    friend ostream &operator<<(ostream &out, Operator *o) { return out << o->tokenString; }
};

class CompoundExpr : public Expr {
protected:
    Operator *op;
    Expr *left, *right; // left will be NULL if unary

public:
    CompoundExpr(Expr *lhs, Operator *op, Expr *rhs);  // for binary
    CompoundExpr(Operator *op, Expr *rhs);             // for unary
    CompoundExpr(Expr *lhs, Operator *op);             // for unary
    void PrintChildren(int indentLevel);
    virtual Type *CheckExpr();
};

class ArithmeticExpr : public CompoundExpr {
public:
    ArithmeticExpr(Expr *lhs, Operator *op, Expr *rhs) : CompoundExpr(lhs, op, rhs) {}
    ArithmeticExpr(Operator *op, Expr *rhs) : CompoundExpr(op, rhs) {}
    const char *GetPrintNameForNode() { return "ArithmeticExpr"; }
};

class RelationalExpr : public CompoundExpr {
public:
    RelationalExpr(Expr *lhs, Operator *op, Expr *rhs) : CompoundExpr(lhs, op, rhs) {}
    const char *GetPrintNameForNode() { return "RelationalExpr"; }
};

class EqualityExpr : public CompoundExpr {
public:
    EqualityExpr(Expr *lhs, Operator *op, Expr *rhs) : CompoundExpr(lhs, op, rhs) {}
    const char *GetPrintNameForNode() { return "EqualityExpr"; }
};

class LogicalExpr : public CompoundExpr {
public:
    LogicalExpr(Expr *lhs, Operator *op, Expr *rhs) : CompoundExpr(lhs, op, rhs) {}
    LogicalExpr(Operator *op, Expr *rhs) : CompoundExpr(op, rhs) {}
    const char *GetPrintNameForNode() { return "LogicalExpr"; }
};

class AssignExpr : public CompoundExpr {
public:
    AssignExpr(Expr *lhs, Operator *op, Expr *rhs) : CompoundExpr(lhs, op, rhs) {}
    const char *GetPrintNameForNode() { return "AssignExpr"; }
    Type *CheckExpr();
};

class PostfixExpr : public CompoundExpr {
public:
    PostfixExpr(Expr *lhs, Operator *op) : CompoundExpr(lhs, op) {}
    const char *GetPrintNameForNode() { return "PostfixExpr"; }
};

class LValue : public Expr {
public:
    LValue(yyltype loc) : Expr(loc) {}
    bool checkLVal()
    {
        return true;
    }
};

class ArrayAccess : public LValue {
protected:
    Expr *base, *subscript;

public:
    ArrayAccess(yyltype loc, Expr *base, Expr *subscript);
    const char *GetPrintNameForNode() { return "ArrayAccess"; }
    void PrintChildren(int indentLevel);
    Type *CheckExpr() {return NULL;}
};

/* Note that field access is used both for qualified names
 * base.field and just field without qualification. We don't
 * know for sure whether there is an implicit "this." in
 * front until later on, so we use one node type for either
 * and sort it out later. */
class FieldAccess : public LValue {
protected:
    Expr *base; // will be NULL if no explicit base
    Identifier *field;

public:
    FieldAccess(Expr *base, Identifier *field); //ok to pass NULL base
    const char *GetPrintNameForNode() { return "FieldAccess"; }
    Type *CheckExpr();
    void PrintChildren(int indentLevel);
};

/* Like field access, call is used both for qualified base.field()
 * and unqualified field().  We won't figure out until later
 * whether we need implicit "this." so we use one node type for either
 * and sort it out later. */
class Call : public Expr {
protected:
    Expr *base; // will be NULL if no explicit base
    Identifier *field;
    List<Expr *> *actuals;

public:
    Call() : Expr(), base(NULL), field(NULL), actuals(NULL) {}
    Call(yyltype loc, Expr *base, Identifier *field, List<Expr *> *args);
    const char *GetPrintNameForNode() { return "Call"; }
    Type *CheckExpr() { return NULL; }
    void PrintChildren(int indentLevel);
};

class ActualsError : public Call {
public:
    ActualsError() : Call() { yyerror(this->GetPrintNameForNode()); }
    const char *GetPrintNameForNode() { return "ActualsError"; }
};

Type *isValidFullExpr(Type *t1, Operator *op, Type *t2);
bool checkLogicOp(Type *t1, Type *t2);
bool checkEqOp(Type *t1, Type *t2);
Type *checkAssignOrArithOp(Type *t1, Operator *op, Type *t2);
bool checkRelOp(Type *t1, Type *t2);
bool isArithOp(Operator *op);
bool isAssignOp(Operator *op);
bool isRelOp(Operator *op);
bool isLogicOp(Operator *op);
bool isEqOp(Operator *op);
bool isValidUnaryExpr(Type *t);
bool checkBoolType(Type *t);
bool checkMatType(Type *t);
bool checkVectorType(Type *t);
bool checkScalarType(Type *t);
bool isOversizedVector(Identifier *swizzle);
bool isSwizzleOutOfBound(Identifier *swizzle, string swizzle_size);
bool isInvalidSwizzle(Identifier *swizzle);

#endif
