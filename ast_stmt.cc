/* File: ast_stmt.cc
 * -----------------
 * Implementation of statement node classes.
 */
#include "ast_stmt.h"
#include "ast_type.h"
#include "ast_decl.h"
#include "ast_expr.h"
#include "errors.h"
#include <iostream>
#include <string.h>
#include <stdio.h>


Program::Program(List<Decl *> *d)
{
    Assert(d != NULL);
    (decls = d)->SetParentAll(this);
}

void Program::PrintChildren(int indentLevel)
{
    decls->PrintAll(indentLevel + 1);
    printf("\n");
}

void Program::Check()
{
    /* pp3: here is where the semantic analyzer is kicked off.
     *      The general idea is perform a tree traversal of the
     *      entire program, examining all constructs for compliance
     *      with the semantic rules.  Eaclass ExprError : public Exprh node can have its own way of
     *      checking itself, which makes for a great use of inheritance
     *      and polymorphism in the node classes.
     */
    Scp *globalScp = new Scp();
    // cout << "Creating new Scp in Program::Check()\n";
    // for (int i = 0; i < decls.NumElements(); i++) {
    //     std::// cout << "decls[" << i << "]: " << decls->Nth(i) << endl;
    // }
    for (int i = 0; i < decls->NumElements(); i++) {
        // // cout<< i ;
        // // cout<< decls->Nth(i);
        decls->Nth(i)->CheckSub();
    }
    // sample test - not the actual working code
    // replace it with your own implementation
    //  if ( decls->NumElements() >= 2 ) {
    // Decl *newDecl  = decls->Nth(1);
    // Decl *prevDecl = decls->Nth(0);
    // ReportError::DeclConflict(newDecl, prevDecl);}
    delete globalScp;
}

void StmtBlock::CheckSub()
{
    //if flag is set to false, create a new scope
    if (!Scp::curr->getShouldntCreateNewScope()) {
        bool isBreak = false;
        bool isContinue = false;
        bool inside_of_default_stmt = false;
        bool inside_of_case_stmt = false;
        bool has_break_inside_case = false;
        bool in_switch = false;

        isBreak = Scp::curr->getCanBreak();
        isContinue = Scp::curr->getCanContinue();
        inside_of_default_stmt  = Scp::curr->getInside_of_default_stmt();
        inside_of_case_stmt  = Scp::curr->getInside_of_case_stmt();
        has_break_inside_case  = Scp::curr->getHas_break_inside_case();
        in_switch  = Scp::curr->getIn_switch();

        Scp *scp = new Scp();
        scp->setCanBreak(isBreak);
        scp->setCanContinue(isContinue);
        scp->setHas_break_inside_case(has_break_inside_case);
        scp->setInside_of_default_stmt(inside_of_default_stmt);
        scp->setInside_of_case_stmt(inside_of_case_stmt);
        scp->setIn_switch(in_switch);
        // cout << "Creating new Scp in StmtBlock::CheckSub()\n";
        for (int i = 0; i < stmts->NumElements(); i++) {
            stmts->Nth(i)->CheckSub();
        }
        Scp::curr->getPrev()->setRetExists(scp->getRetExists());
        delete scp;
    }
    //else if flag is set to true, set the flag back to false
    else {
        Scp::curr->setShouldntCreateNewScope(false);
        // cout << "Not creating a new scope in StmtBlock::CheckSub()\n";
        for (int i = 0; i < stmts->NumElements(); i++) {
            stmts->Nth(i)->CheckSub();
        }
        // bool exists = scp->getRetExists();
        // delete scp;
        // Scp::curr->setRetExists(exists);
    }
}
StmtBlock::StmtBlock(List<VarDecl *> *d, List<Stmt *> *s)
{
    Assert(d != NULL && s != NULL);
    (decls = d)->SetParentAll(this);
    (stmts = s)->SetParentAll(this);
}

void StmtBlock::PrintChildren(int indentLevel)
{
    decls->PrintAll(indentLevel + 1);
    stmts->PrintAll(indentLevel + 1);
}

DeclStmt::DeclStmt(Decl *d)
{
    Assert(d != NULL);
    (decl = d)->SetParent(this);
}

void DeclStmt::PrintChildren(int indentLevel)
{
    decl->Print(indentLevel + 1);
}

void DeclStmt::CheckSub()
{
    decl->CheckSub();
}

ConditionalStmt::ConditionalStmt(Expr *t, Stmt *b)
{
    Assert(t != NULL && b != NULL);
    (test = t)->SetParent(this);
    (body = b)->SetParent(this);
}

ForStmt::ForStmt(Expr *i, Expr *t, Expr *s, Stmt *b): LoopStmt(t, b)
{
    Assert(i != NULL && t != NULL && b != NULL);
    (init = i)->SetParent(this);
    step = s;
    if (s) {
        (step = s)->SetParent(this);
    }
}

void ForStmt::CheckSub()
{
    init->CheckSub();
    // cout << "Checked init in ForStmt::CheckSub()\n";

    //check if test isn't of a boolean type
    if (strcmp(test->CheckExpr()->getName(), "bool") != 0) {
        // cout << test->CheckExpr()->getName() << " is the test string in ForStmt::CheckSub() \n";
        ReportError::TestNotBoolean(test);
    }
    // cout << "Checked test in ForStmt::CheckSub()\n";

    step->CheckSub(); //call checksub on all parts of for statement
    // cout << "Checked step in ForStmt::CheckSub()\n";


    Scp *forScope = new Scp();
    forScope->setCanBreak(true);   //set break and continue bools
    // cout << "setCanBreak is now true in ForStmt::CheckSub()\n";

    forScope->setCanContinue(true);
    // cout << "Creating new Scp in ForStmt::CheckSub()\n";

    body->CheckSub();
    // cout << "Checked body in ForStmt::CheckSub()\n";

    //setting retExists in case it was set to true at some point inside for scope
    Scp::curr->getPrev()->setRetExists(forScope->getRetExists());
    delete forScope;

}

void ForStmt::PrintChildren(int indentLevel)
{
    init->Print(indentLevel + 1, "(init) ");
    test->Print(indentLevel + 1, "(test) ");
    if (step) {
        step->Print(indentLevel + 1, "(step) ");
    }
    body->Print(indentLevel + 1, "(body) ");
}

void WhileStmt::PrintChildren(int indentLevel)
{
    test->Print(indentLevel + 1, "(test) ");
    body->Print(indentLevel + 1, "(body) ");
}

void WhileStmt::CheckSub()
{
    Scp *whileScope = new Scp();
    whileScope->setCanBreak(true);   //set break and continue bools
    whileScope->setCanContinue(true);

    //test is an expr
    if (strcmp(test->CheckExpr()->getName(), "bool") != 0) {
        ReportError::TestNotBoolean(test);
    }

    body->CheckSub(); //body is stmt

    Scp::curr->getPrev()->setRetExists(whileScope->getRetExists());
    delete whileScope;

}

IfStmt::IfStmt(Expr *t, Stmt *tb, Stmt *eb): ConditionalStmt(t, tb)
{
    Assert(t != NULL && tb != NULL); // else can be NULL
    elseBody = eb;
    if (elseBody) { elseBody->SetParent(this); }
}

void IfStmt::PrintChildren(int indentLevel)
{
    if (test) { test->Print(indentLevel + 1, "(test) "); }
    if (body) { body->Print(indentLevel + 1, "(then) "); }
    if (elseBody) { elseBody->Print(indentLevel + 1, "(else) "); }
}

void IfStmt::CheckSub()
{
    //test is an expr
    if (strcmp(test->CheckExpr()->getName(), "bool") != 0) {
        ReportError::TestNotBoolean(test);
    }
    // cout << "Creating a new scope in IfStmt::CheckSub()\n";
    Scp *ifScope = new Scp();
    body->CheckSub(); //body is stmt
    // cout << "Checked body in IfStmt::CheckSub()\n";
    //check if if part of statement had a return stmt
    bool ifReturn = false;
    if (ifScope->getRetExists()) {
        ifReturn = true;
    }
    delete ifScope;

    bool elseReturn = false;
    if (elseBody != NULL) {
        Scp *elseScope = new Scp();
        elseBody->CheckSub(); //elseBody is stmt
        //check if else part of statement had a return stmt

        if (elseScope->getRetExists()) {
            elseReturn = true;
        }
        delete elseScope;
    }

    if (ifReturn && elseReturn)
        //set curr scope (one that if and else are inside of) retExists bool to true
    {
        Scp::curr->setRetExists(true);
    }

}

ReturnStmt::ReturnStmt(yyltype loc, Expr *e) : Stmt(loc)
{
    expr = e;
    if (e != NULL) { expr->SetParent(this); }
}

void ReturnStmt::PrintChildren(int indentLevel)
{
    if (expr) {
        expr->Print(indentLevel + 1);
    }
}

void ReturnStmt::CheckSub()
{
    // cout << "Entering ReturnStmt in ReturnStmt::CheckSub()\n";
    if (Scp::curr->getIn_switch()) {
        // cout << "In switch stmt inside of ReturnStmt::CheckSub()\n";
        if (Scp::curr->getInside_of_default_stmt()) {
            // cout << "In switch default inside of ReturnStmt::CheckSub()\n";
        }
        if (!Scp::curr->getHas_break_inside_case()) {
            // cout << "Case statement had a break in ReturnStmt::CheckSub()\n";
        }
        if (!Scp::curr->getHas_break_inside_case()
                && Scp::curr->getInside_of_default_stmt()) {
            Scp::curr->setRetExists(true);
        } else {
            Type *expected = Scp::curr->getRetType();
            Type *given;
            if (NULL != expr) {
                given = expr->CheckExpr();
            } else {
                given = new Type("void");
            }

            // cout << "Expected: " << expected->getNameStr() << endl;
            // cout << "Given: " << given->getNameStr() << " in ReturnStmt::CheckSub()\n";
            if (expected->getNameStr() != given->getNameStr()) {
                ReportError::ReturnMismatch(this, given, expected);
            }
            return;
        }
    } else {
        Scp::curr->setRetExists(true);
    }
    //compare the return type in the statement with the return type in the fndecl
    //if they are not equal, report error
    Type *expected = Scp::curr->getRetType();

    Type *given;
    if (NULL != expr) {
        given = expr->CheckExpr();
    } else {
        given = new Type("void");
    }

    // cout << "Expected: " << expected->getNameStr() << endl;
    // cout << "Given: " << given->getNameStr() << " in ReturnStmt::CheckSub()\n";
    if (expected->getNameStr() != given->getNameStr()) {
        ReportError::ReturnMismatch(this, given, expected);
    }
    return;
}

SwitchLabel::SwitchLabel(Expr *l, Stmt *s)
{
    Assert(l != NULL && s != NULL);
    (label = l)->SetParent(this);
    (stmt = s)->SetParent(this);
}

SwitchLabel::SwitchLabel(Stmt *s)
{
    Assert(s != NULL);
    label = NULL;
    (stmt = s)->SetParent(this);
}

void SwitchLabel::PrintChildren(int indentLevel)
{
    if (label) { label->Print(indentLevel + 1); }
    if (stmt) { stmt->Print(indentLevel + 1); }
}

SwitchStmt::SwitchStmt(Expr *e, List<Stmt *> *c, Default *d)
{
    Assert(e != NULL && c != NULL && c->NumElements() != 0);
    (expr = e)->SetParent(this);
    (cases = c)->SetParentAll(this);
    def = d;
    if (def) { def->SetParent(this); }
}

void SwitchStmt::PrintChildren(int indentLevel)
{
    if (expr) { expr->Print(indentLevel + 1); }
    if (cases) { cases->PrintAll(indentLevel + 1); }
    if (def) { def->Print(indentLevel + 1); }
}

void BreakStmt::CheckSub()
{
    // cout << "Entering BreakStmt::CheckSub()\n";
    //not inside of a LoopStmt
    // cout << "canBreak is" << Scp::curr->getCanBreak() << "in BreakStmt::CheckSub()\n";
    if (!Scp::curr->getCanBreak()) {
        ReportError::BreakOutsideLoop(this);
    }
    if (Scp::curr->getInside_of_case_stmt()) {
        // cout << "Break stmt found inside of a case stmt\n";
        Scp::curr->setHas_break_inside_case(true);
    }
}

void ContinueStmt::CheckSub()
{
    //not inside of a LoopStmt
    if (!Scp::curr->getCanContinue()) {
        ReportError::ContinueOutsideLoop(this);
    }
}

void Case::CheckSub()
{
    // cout << "Entering Case::CheckSub()\n";
    //label is of type Expr*
    string label_type = label->CheckExpr()->getNameStr();
    // if (label_type != "int" && label_type != "float" && label_type != "bool")  {
    //     ReportError::CaseLabelNotInt(label);
    // }
    Scp::curr->setInside_of_case_stmt(true);
    //stmt is of type Stmt*, call CheckSub()
    stmt->CheckSub();
    Scp::curr->setInside_of_case_stmt(false);
}

void Default::CheckSub()
{
    Scp::curr->setInside_of_default_stmt(true);
    // cout << "Entering Default::CheckSub()\n";
    //stmt is of type Stmt*, call CheckSub()
    stmt->CheckSub();
    Scp::curr->setInside_of_default_stmt(false);

}

void SwitchStmt::CheckSub()
{
    //expr is of type Expr*
    string expr_type = expr->CheckExpr()->getNameStr();
    // if (expr_type != "int" && expr_type != "float" && expr_type != "bool")  {
    //     ReportError::SwitchExprNotInt(expr);
    // }

    Scp *switchScope = new Scp();
    switchScope->setCanBreak(true);
    switchScope->setIn_switch(true);
    // // cout << "setCanBreak is now true in SwitchStmt::CheckSub()\n";

    // switchScope->setCanContinue(true);
    // cout << "Creating new Scp in SwitchStmt::CheckSub()\n";

    //List<Stmt *> *cases
    for (int i = 0; i < cases->NumElements(); i++) {
        cases->Nth(i)->CheckSub();
    }

    //Default *def
    if (def != NULL) {
        def->CheckSub();
    }

    if (!Scp::curr->getPrev()->getRetExists()) {
        // cout << "Inside SwitchStmt::CheckSub() setting prev retExists to retExists of switch scope.\n";
        Scp::curr->getPrev()->setRetExists(switchScope->getRetExists());
    } /*else {
        Scp::curr->setRetExists(switchScope->getRetExists());
    }*/
    delete switchScope;

}

