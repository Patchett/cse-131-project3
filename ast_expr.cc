/* File: ast_expr.cc
 * -----------------
 * Implementation of expression node classes.
 */

#include <string.h>
#include "ast_expr.h"
#include "ast_type.h"
#include "ast_decl.h"


IntConstant::IntConstant(yyltype loc, int val) : Expr(loc)
{
    value = val;
}
void IntConstant::PrintChildren(int indentLevel)
{
    printf("%d", value);
}

FloatConstant::FloatConstant(yyltype loc, double val) : Expr(loc)
{
    value = val;
}
void FloatConstant::PrintChildren(int indentLevel)
{
    printf("%g", value);
}

BoolConstant::BoolConstant(yyltype loc, bool val) : Expr(loc)
{
    value = val;
}
void BoolConstant::PrintChildren(int indentLevel)
{
    printf("%s", value ? "true" : "false");
}

VarExpr::VarExpr(yyltype loc, Identifier *ident) : Expr(loc)
{
    Assert(ident != NULL);
    this->id = ident;
}

void VarExpr::PrintChildren(int indentLevel)
{
    id->Print(indentLevel + 1);
}

Operator::Operator(yyltype loc, const char *tok) : Node(loc)
{
    Assert(tok != NULL);
    strncpy(tokenString, tok, sizeof(tokenString));
}

void Operator::PrintChildren(int indentLevel)
{
    printf("%s", tokenString);
}

Type *AssignExpr::CheckExpr()
{
    // cout << "Starting AssignExpr::CheckExpr()\n";
    Type *l_type = NULL;
    Type *r_type = NULL;
    if (left)  { l_type = left->CheckExpr();  }
    if (right) { r_type = right->CheckExpr(); }
    if ((NULL == l_type) /*|| !(left->checkLVal())*/) {
        // cout << "l_type was NULL or was not an LValue\n";
        ReportError::IncompatibleOperands(op, l_type, r_type);
        return new Type("error");
    } else {
        Type *t = isValidFullExpr(l_type, op, r_type);
        if (NULL == t) {
            // cout << "AssignExpr did not pass isValidFullExpr()\n";
            ReportError::IncompatibleOperands(op, l_type, r_type);
            return new Type("error");
        } else {
            return t;
        }
    }
}

Type *CompoundExpr::CheckExpr()
{
    // cout << "Starting Compound::CheckExpr()\n";
    Type *l_type = NULL;
    Type *r_type = NULL;

    if (left)  { l_type = left->CheckExpr(); }
    if (right) {
        r_type = right->CheckExpr();
        // cout << "r_type is " << r_type << " in Compound::CheckExpr()\n";
    }
    if (NULL == l_type) {
        // cout << "l_type is null in Compound::CheckExpr()\n";
        if (isValidUnaryExpr(r_type)) {
            return r_type;
        } else {
            ReportError::IncompatibleOperand(op, r_type);
            return new Type("error");
        }
    } else if (NULL == r_type) {
        // cout << "r_type is null in Compound::CheckExpr()\n";
        if (isValidUnaryExpr(l_type)) {
            return l_type;
        } else {
            ReportError::IncompatibleOperandLHS(op, l_type);
            return new Type("error");
        }
    } else {
        // cout << "l_type and r_type are not null in Compound::CheckExpr()\n";
        Type *t = isValidFullExpr(l_type, op, r_type);
        if (NULL != t) {
            return t;
        } else {
            ReportError::IncompatibleOperands(op, l_type, r_type);
            return new Type("error");
        }
    }
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o, Expr *r)
    : Expr(Join(l->GetLocation(), r->GetLocation()))
{
    Assert(l != NULL && o != NULL && r != NULL);
    (op = o)->SetParent(this);
    (left = l)->SetParent(this);
    (right = r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Operator *o, Expr *r)
    : Expr(Join(o->GetLocation(), r->GetLocation()))
{
    Assert(o != NULL && r != NULL);
    left = NULL;
    (op = o)->SetParent(this);
    (right = r)->SetParent(this);
}

CompoundExpr::CompoundExpr(Expr *l, Operator *o)
    : Expr(Join(l->GetLocation(), o->GetLocation()))
{
    Assert(l != NULL && o != NULL);
    (left = l)->SetParent(this);
    (op = o)->SetParent(this);
}

void CompoundExpr::PrintChildren(int indentLevel)
{
    if (left) { left->Print(indentLevel + 1); }
    op->Print(indentLevel + 1);
    if (right) { right->Print(indentLevel + 1); }
}

ArrayAccess::ArrayAccess(yyltype loc, Expr *b, Expr *s) : LValue(loc)
{
    (base = b)->SetParent(this);
    (subscript = s)->SetParent(this);
}

void ArrayAccess::PrintChildren(int indentLevel)
{
    base->Print(indentLevel + 1);
    subscript->Print(indentLevel + 1, "(subscript) ");
}

// static void InaccessibleSwizzle(Identifier * swizzle, Expr * base);
// static void InvalidSwizzle(Identifier * swizzle, Expr * base);
// static void SwizzleOutOfBound(Identifier * swizzle, Expr * base);
// static void OversizedVector(Identifier * swizzle, Expr * base);

Type *FieldAccess::CheckExpr()
{
    Type *t = NULL;
    string swizzle_size;
    if (NULL != base) {
        t = base->CheckExpr();
        if (checkVectorType(t)) {
            swizzle_size = (t->getNameStr().substr(3, 1));
            if (isInvalidSwizzle(field)) {
                ReportError::InvalidSwizzle(field, base);
                return new Type("error");
            } else if (isSwizzleOutOfBound(field, swizzle_size)) {
                ReportError::SwizzleOutOfBound(field, base);
                return new Type("error");
            } else if (isOversizedVector(field)) {
                ReportError::OversizedVector(field, base);
                return new Type("error");
            } else {
                if (field->getNameStr().length() == 1) {
                    return new Type("float");
                } else {
                    // cout << "In FieldAccess::CheckExpr() returning type: " << (t->getNameStr().substr(0, 3) + std::to_string((long long unsigned int)field->getNameStr().length())) << " for " << field->getNameStr() <<  endl;
                    return new Type(((t->getNameStr().substr(0, 3) + std::to_string((long long unsigned int)field->getNameStr().length())).c_str()));
                }
            }
        } else if (t->getNameStr() == "error") {
            return new Type("error"); // Don't report the error again
        } else {
            ReportError::InaccessibleSwizzle(field, base);
            return new Type("error");
        }
    } else {
        // May need to add a call to a ReportError function here
        return new Type("error");
    }
}

bool isOversizedVector(Identifier *swizzle)
{
    string letters = swizzle->getNameStr();
    if (letters.length() >= 5) {
        // cout << "isOversizedVector returned true because swizzle length was: " << letters.length() << endl;
        return true;
    } else {
        return false;
    }
}

bool isSwizzleOutOfBound(Identifier *swizzle, string swizzle_size)
{
    string letters = swizzle->getNameStr();
    if (swizzle_size == "2") {
        for (int i = 0; i < letters.length(); i++) {
            if (letters[i] != 'x' && letters[i] != 'y') {
                // cout << "isSwizzleOutOfBound returned true on a vec2 because it found: " << letters[i] << endl;
                return true;
            }
        }
        return false;
    } else if (swizzle_size == "3") {
        for (int i = 0; i < letters.length(); i++) {
            if (letters[i] != 'x' && letters[i] != 'y' && letters[i] != 'z') {
                // cout << "isSwizzleOutOfBound returned true on a vec3 because it found: " << letters[i] << endl;
                return true;
            }
        }
        return false;
    } else if (swizzle_size == "4") {
        return isInvalidSwizzle(swizzle);
    } else {
        // cout << "isSwizzleOutOfBound returned true because swizzle_size was not valid.\n";
        return true;
    }
}

bool isInvalidSwizzle(Identifier *swizzle)
{
    string letters = swizzle->getNameStr();
    for (int i = 0; i < letters.length(); i++) {
        if (letters[i] != 'w' && letters[i] != 'x' && letters[i] != 'y' && letters[i] != 'z') {
            // cout << "isInvalidSwizzle returned true because identifier was '" << letters[i] << "' and not in the set [xyzw].\n";
            return true;
        }
    }
    return false;
}

FieldAccess::FieldAccess(Expr *b, Identifier *f)
    : LValue(b ? Join(b->GetLocation(), f->GetLocation()) : * f->GetLocation())
{
    Assert(f != NULL); // b can be be NULL (just means no explicit base)
    base = b;
    if (base) { base->SetParent(this); }
    (field = f)->SetParent(this);
}

void FieldAccess::PrintChildren(int indentLevel)
{
    if (base) { base->Print(indentLevel + 1); }
    field->Print(indentLevel + 1);
}

Call::Call(yyltype loc, Expr *b, Identifier *f, List<Expr *> *a) : Expr(loc)
{
    Assert(f != NULL && a != NULL); // b can be be NULL (just means no explicit base)
    base = b;
    if (base) { base->SetParent(this); }
    (field = f)->SetParent(this);
    (actuals = a)->SetParentAll(this);
}

void Call::PrintChildren(int indentLevel)
{
    if (base) { base->Print(indentLevel + 1); }
    if (field) { field->Print(indentLevel + 1); }
    if (actuals) { actuals->PrintAll(indentLevel + 1, "(actuals) "); }
}

Type *isValidFullExpr(Type *t1, Operator *op, Type *t2)
{
    // cout << "Entering isValidFullExpr\n";
    if (isArithOp(op) || isAssignOp(op)) {
        return checkAssignOrArithOp(t1, op, t2);
    } else if (isRelOp(op)) {
        if (checkRelOp(t1, t2)) {
            return new Type("bool");
        } else {
            return NULL;
        }
    } else if (isLogicOp(op)) {
        if (checkLogicOp(t1, t2)) {
            return new Type("bool");
        } else {
            return NULL;
        }
    } else if (isEqOp(op)) {
        if (checkEqOp(t1, t2)) {
            return new Type("bool");
        } else {
            return NULL;
        }
    } else {
        return NULL;
    }
}

bool checkLogicOp(Type *t1, Type *t2)
{
    return checkBoolType(t1) && checkBoolType(t2);
}

bool checkEqOp(Type *t1, Type *t2)
{
    return (t1->getNameStr() == t2->getNameStr());
}

Type *checkAssignOrArithOp(Type *t1, Operator *op, Type *t2)
{
    if (op->getTokStr() == "=") {
        // cout << "Found = operator in checkAssignOrArithOp" << endl;
        if ((t1->getNameStr() == t2->getNameStr())) {
            return t1;
        } else {
            return NULL;
        }
    } else if (op->getTokStr() == "*=") {
        if (checkVectorType(t1)) {
            if ((t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1)) || t2->getNameStr() == "float") {
                return t1;
            } else {
                return NULL;
            }
        } else if (checkMatType(t1)) {
            if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
                return t1;
            } else {
                return NULL;
            }
        } else if (checkScalarType(t1)) {
            if (t1->getNameStr() == t2->getNameStr()) {
                return t1;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    } else if (op->getTokStr() == "*") {
        if (checkVectorType(t1)) {
            if (checkMatType(t2)) {
                if ((t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1))) {
                    return t1;
                } else {
                    return NULL;
                }
            } else if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
                return t1;
            } else {
                return NULL;
            }
        } else if (checkMatType(t1)) {
            if (checkVectorType(t2)) {
                if (t2->getNameStr().substr(3, 1) == t1->getNameStr().substr(3, 1)) {
                    return t2;
                } else {
                    return NULL;
                }
            } else if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
                return t1;
            } else {
                return NULL;
            }
        } else if (checkScalarType(t1)) {
            if (t1->getNameStr() == t2->getNameStr()) {
                return t1;
            } else if (checkMatType(t2) || checkVectorType(t2)) {
                return t2;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    } else {
        if (checkMatType(t1) || checkVectorType(t1)) {
            if ((t2->getNameStr() == "float") || (t1->getNameStr() == t2->getNameStr())) {
                return t1;
            } else {
                return NULL;
            }
        } else if (checkMatType(t2) || checkVectorType(t2)) {
            if ((t1->getNameStr() == "float") && (op->getTokStr().find("=") != string::npos)) {
                return NULL;
            } else if ((t1->getNameStr() == "float") && (op->getTokStr().find("=") == string::npos)) {
                return t2;
            } else if (t1->getNameStr() == t2->getNameStr()) {
                return t1;
            } else {
                return NULL;
            }
        } else if (checkScalarType(t1)) {
            if (t1->getNameStr() == t2->getNameStr()) {
                return t1;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }
}

bool checkRelOp(Type *t1, Type *t2)
{
    // cout << "checkRelOp returned " << (checkScalarType(t1) && (t1->getNameStr() == t2->getNameStr())) << endl;
    return (checkScalarType(t1) && (t1->getNameStr() == t2->getNameStr()));
}

bool isArithOp(Operator *op)
{
    return ((op->getTokStr() == "+") || (op->getTokStr() == "-") || (op->getTokStr() == "*") || (op->getTokStr() == "/"));
}

bool isAssignOp(Operator *op)
{
    return op->getTokStr() == "=" || op->getTokStr() == "*=" || op->getTokStr() == "/=" || op->getTokStr() == "-=" || op->getTokStr() == "+=";
}

bool isRelOp(Operator *op)
{
    return op->getTokStr() == "<" || op->getTokStr() == ">" || op->getTokStr() == ">=" || op->getTokStr() == "<=";
}

bool isLogicOp(Operator *op)
{
    return op->getTokStr() == "&&" || op->getTokStr() == "||";
}

bool isEqOp(Operator *op)
{
    return op->getTokStr() == "!=" || op->getTokStr() == "==";
}

bool isValidUnaryExpr(Type *t)
{
    return checkVectorType(t) || checkScalarType(t) || checkMatType(t);
}

bool checkBoolType(Type *t)
{
    return t->getNameStr() == "bool";
}

bool checkMatType(Type *t)
{
    return t->getNameStr() == "mat2" || t->getNameStr() == "mat3" || t->getNameStr() == "mat4";
}

bool checkVectorType(Type *t)
{
    return t->getNameStr() == "vec2" || t->getNameStr() == "vec3" || t->getNameStr() == "vec4";
}

bool checkScalarType(Type *t)
{
    return t->getNameStr() == "int" || t->getNameStr() == "float";
}
